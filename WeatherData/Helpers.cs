﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherData.DTO;

namespace WeatherData
{
    public class Helpers
    {
        public static string GetDate(string Time)
        {
            var date = Convert.ToDateTime(Time);
            string st = "";
            if (date.Month.ToString().Length == 1)
                st = $"{date.Day}.0{date.Month}";
            else
                st = $"{date.Day}.{date.Month}";
            return st;
        }

        public static string GetDateAndTime(string Time)
        {
            var date = Convert.ToDateTime(Time);
            string st = "";
            st = $"{date.Hour}";
            return st;
        }

        public static void GetRightData(List<ForecastItem> forecastItems, ref List<ForecastItem> list1, ref List<ForecastItem> list2,
            ref List<ForecastItem> list3, ref List<ForecastItem> list4, ref List<ForecastItem> list5)
        {
            list1 = new List<ForecastItem>();
            list2 = new List<ForecastItem>();
            list3 = new List<ForecastItem>();
            list4 = new List<ForecastItem>();
            list5 = new List<ForecastItem>();
            int i = 0;
            var time = Convert.ToDateTime(forecastItems[i].Time);
            while (time.Day == DateTime.Now.Day)
            {
                time = Convert.ToDateTime(forecastItems[i].Time);
                list1.Add(forecastItems[i]);
                i++;
            }
            while (time.Day == DateTime.Now.AddDays(1).Day)
            {
                time = Convert.ToDateTime(forecastItems[i].Time);
                list2.Add(forecastItems[i]);
                list1.Add(forecastItems[i]);
                i++;
            }
            while (time.Day == DateTime.Now.AddDays(2).Day)
            {
                time = Convert.ToDateTime(forecastItems[i].Time);
                list3.Add(forecastItems[i]);
                i++;
            }
            while (time.Day == DateTime.Now.AddDays(3).Day)
            {
                time = Convert.ToDateTime(forecastItems[i].Time);
                list4.Add(forecastItems[i]);
                i++;
            }
            while (time.Day == DateTime.Now.AddDays(4).Day && i != forecastItems.Count)
            {
                time = Convert.ToDateTime(forecastItems[i].Time);
                list5.Add(forecastItems[i]);
                i++;
            }
        }
        public static List<NewForecastItem> ReturnListOfNewItemsForForecast (List<ForecastItem> forecastItems, List<ForecastItem> list1, List<ForecastItem> list2,
            List<ForecastItem> list3, List<ForecastItem> list4, List<ForecastItem> list5)
        {
            var list1New = new List<ForecastItem>();
            int i = 0;
            while (list1[i].TimeToString != list2[0].TimeToString)
            {
                list1New.Add(list1[i]);
                i++;
            }
            var list = new List<NewForecastItem>
                {
                new NewForecastItem
                {
                    WindSpeed = WindSpeed(list1New),
                    Pressure = Pressure(list1New),
                    Time = list1New[0].TimeToString,
                    TempMax = TempMax(list1New),
                    TempMin = TempMin(list1New)
                },
                new NewForecastItem
                {
                    WindSpeed = WindSpeed(list2),
                    Pressure = Pressure(list2),
                    Time = list2[0].TimeToString,
                    TempMax = TempMax(list2),
                    TempMin = TempMin(list2)
                },
                new NewForecastItem
                {
                    WindSpeed = WindSpeed(list3),
                    Pressure = Pressure(list3),
                    Time = list3[0].TimeToString,
                    TempMax = TempMax(list3),
                    TempMin = TempMin(list3)
                },
                new NewForecastItem
                {
                    WindSpeed = WindSpeed(list4),
                    Pressure = Pressure(list4),
                    Time = list4[0].TimeToString,
                    TempMax = TempMax(list4),
                    TempMin = TempMin(list4)
                },
                new NewForecastItem
                {
                    WindSpeed = WindSpeed(list5),
                    Pressure = Pressure(list5),
                    Time = list5[0].TimeToString,
                    TempMax = TempMax(list5),
                    TempMin = TempMin(list5)
                }

            };
            
            return list;
        }

        public static double TempMin(List<ForecastItem> list)
        {
            double tempMin = list[0].Main.TempMin;
            foreach (var i in list)
            {
                if (i.Main.TempMin < tempMin) tempMin = i.Main.TempMin;                
            }
            return Math.Round(tempMin);
        }

        public static double TempMax(List<ForecastItem> list)
        {
            double tempMax = list[0].Main.TempMax;
            foreach (var i in list)
            {
                if (i.Main.TempMax > tempMax) tempMax = i.Main.TempMax;
            }
            return Math.Round(tempMax);
        }
        public static int WindSpeed(List<ForecastItem> list)
        {
            double speed=0;
            int count = 0;
            foreach(var i in list)
            {
                speed += i.Wind.WindSpeed;
                count++;
            }
            return (int)speed / count;
        }
        public static int Pressure(List<ForecastItem> list)
        {
            double p = 0;
            int count = 0;
            foreach (var i in list)
            {
                p += i.Main.Pressure;
                count++;
            }
            return (int)p / count;
        }
    }
}
