﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WeatherData.DTO;
using WeatherData.DTO.CurrentWeather;
using WeatherData.Interfaces;

namespace WeatherData
{
    public class OpenWeatherMapData : IData
    {
        public ForecastData Forecast { get; set; }
        public CurrentWeatherDataResults Weather { get; set; }

        const string AppID = "84c0a758afe0685f6b88a7be260dbc3d";

        public OpenWeatherMapData()
        {

        }

        public string BuildUrl(string baseUrl, IDictionary<string, string> parameters)
        {
            var sb = new StringBuilder(baseUrl);
            if (parameters?.Count > 0)
            {
                sb.Append('?');
                foreach (var p in parameters)
                {
                    sb.Append(p.Key);
                    sb.Append("=");
                    sb.Append(WebUtility.UrlEncode(p.Value));
                    sb.Append('&');
                }
                sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }

        public string MakeQuery(string city, string typeOfQuery)
        {
            return BuildUrl("http://api.openweathermap.org/data/2.5/" + typeOfQuery, new Dictionary<string, string>
            {
                {"q", city },
                {"units", "metric" },
                {"appid", AppID }
            });
        }

        public async Task<ForecastData> GetForecast(string city)
        {
            using (var client = new HttpClient())
            {
                string result = await client.GetStringAsync(MakeQuery(city, "forecast"));  // Blocking call!
                return JsonConvert.DeserializeObject<ForecastData>(result);
            }
        }

        public async Task<CurrentWeatherDataResults> GetWeather(string city)
        {
            using (var client = new HttpClient())
            {
                string result = await client.GetStringAsync(MakeQuery(city, "weather"));  // Blocking call!
                return JsonConvert.DeserializeObject<CurrentWeatherDataResults>(result);
            }
        }
    }
}
