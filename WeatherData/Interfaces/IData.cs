﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherData.DTO;
using WeatherData.DTO.CurrentWeather;

namespace WeatherData.Interfaces
{
    public interface IData
    {
        ForecastData Forecast { get; set; }
        CurrentWeatherDataResults Weather { get; set; }
        Task<CurrentWeatherDataResults> GetWeather(string city);
        Task<ForecastData> GetForecast(string city);
        string BuildUrl(string baseUrl, IDictionary<string, string> parameters);
        string MakeQuery(string city, string typeOfQuery);
    }
}
