﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData
{
    public class NewForecastItem
    {
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public int WindSpeed { get; set; }
        public string Time { get; set; }
        public int Pressure { get; set; }

    }
}
