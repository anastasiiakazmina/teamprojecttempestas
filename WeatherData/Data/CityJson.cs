﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData.Data
{
    public class CityJson
    {
        public string country { get; set; }
        public string name { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
