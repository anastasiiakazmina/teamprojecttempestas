﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherData.Interfaces;

namespace WeatherData
{
    public class Factory
    {
        private static Factory _instance;

        private Factory() { }

        public static Factory Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Factory();
                return _instance;
            }
        }

        private IData WeatherData;
        public IData CreateWeatherData()
        {
            return WeatherData ?? (WeatherData = new OpenWeatherMapData());
        }
    }
}
