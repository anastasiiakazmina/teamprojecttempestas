﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData.DTO
{
    public class Weather
    {
        [JsonProperty("main")]
        public string Description { get; set; }
        [JsonProperty("description")]
        public string Details { get; set; }
    }
}
