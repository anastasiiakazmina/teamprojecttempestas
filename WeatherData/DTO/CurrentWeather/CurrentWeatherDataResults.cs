﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData.DTO.CurrentWeather
{
    public class CurrentWeatherDataResults
    {
        [JsonProperty("main")]
        public WeatherDataElements Main { get; set; }
        [JsonProperty("wind")]
        public Wind WindInfo { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("weather")]
        public Weather[] Weather { get; set; }
    }
}
