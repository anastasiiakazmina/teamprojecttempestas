﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData.DTO
{
    public class ForecastResults
    {
        public WeatherDataElements Main { get; set; }
        public Weather Weather { get; set; }
        public string Time { get; set; }
        public Wind Wind { get; set; }
    }
}
