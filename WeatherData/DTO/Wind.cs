﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WeatherData.DTO
{
    public class Wind
    {
        [JsonProperty("speed")]
        public double WindSpeed { get; set; }
        [JsonProperty("deg")]
        public double Deg { get; set; }
    }
}
