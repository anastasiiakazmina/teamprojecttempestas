﻿using Newtonsoft.Json;

namespace WeatherData.DTO
{
    public class City
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}