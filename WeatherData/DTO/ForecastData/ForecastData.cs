﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData.DTO
{
    public class ForecastData
    {
        [JsonProperty("list")]
        public List<ForecastItem> Forecast { get; set; }
    }
}
