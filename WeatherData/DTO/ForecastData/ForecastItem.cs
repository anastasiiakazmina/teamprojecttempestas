﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherData.DTO
{
    public class ForecastItem
    {
        [JsonProperty("main")]
        public WeatherDataElements Main { get; set; }
        [JsonProperty("weather")]
        public Weather[] Weather { get; set; }
        [JsonProperty("dt_txt")]
        public string Time { get; set; }
        [JsonProperty("wind")]
        public Wind Wind { get; set; }
        [JsonIgnore]
        public string TimeToString
        {
            get
            {
                return Helpers.GetDate(Time);
            }
        }

    }
}
