﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WeatherData;
using WeatherData.DTO;
using WeatherData.Interfaces;
using WeatherData.Data;

namespace TeamProject
{
    /// <summary>
    /// Логика взаимодействия для WeatherStatistics.xaml
    /// </summary>
    public partial class WeatherStatistics : Page
    {
        string CityTitle;
        string sm;
        IData wer = Factory.Instance.CreateWeatherData();
        List<CityJson> cities = new List<CityJson>();
        List<string> strings = new List<string>();

        public WeatherStatistics()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
            cities = RestoreList<CityJson>("Data/cities.json");
            foreach (var item in cities)
            {
                strings.Add($"{item.name}, {item.country}");
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DateTime currentTime = DateTime.Now;
            if (currentTime.Minute.ToString().Length == 1)
            {
                if (currentTime.Hour.ToString().Length == 1)
                    Time.Text = $"0{currentTime.Hour}:0{currentTime.Minute}";
                else
                    Time.Text = $"{currentTime.Hour}:0{currentTime.Minute}";
            }
            else
            {
                if (currentTime.Hour.ToString().Length == 1)
                    Time.Text = $"0{currentTime.Hour}:{currentTime.Minute}";
                else
                    Time.Text = $"{currentTime.Hour}:{currentTime.Minute}";
            }
        }

        private async Task ShowWeather()
        {
            string[] smnew = sm.Split(',');
            var city = smnew[0];
            try
            {
                var weatherData = await wer.GetWeather(city);

                if (CityName != null)
                {
                    Description.Text = weatherData.Weather[0].Description;
                    CityName.Text = weatherData.Name;
                    HumidityValue.Text = $"{Math.Round(weatherData.Main.Humidity)}%";
                    Humidity.Visibility = Visibility.Visible;
                    WindyValue.Text = $"{Math.Round(weatherData.WindInfo.WindSpeed)}m/s";
                    Windy.Visibility = Visibility.Visible;
                    PressureValue.Text = $"{Math.Round(weatherData.Main.Pressure)}Pa";
                    Pressure.Visibility = Visibility.Visible;
                    //this.Background = new ImageBrush(new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), "Images\\rainy.jpg")));
                    WeatherDataTextBlock.Text = $"{Math.Round(weatherData.Main.Temp)}°";
                    CityTitle = weatherData.Name;
                    CB.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("No DATA returned", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    CityName.Text = null;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occured: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                CityName.Text = null;
                return;
            }
        }

        public static List<T> RestoreList<T>(string fileName)
        {
            using (var sr = new StreamReader(fileName))
            {
                using (var jsonReader = new JsonTextReader(sr))
                {
                    var serializer = new JsonSerializer();
                    return serializer.Deserialize < List < T>>(jsonReader);
                }
            }
        }

        private void CB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CB.Text != null)
            {
                string textToSearch = CB.Text.ToLower();
                if (String.IsNullOrEmpty(textToSearch))
                    return;
                string[] result = (from i in strings
                                   where i.ToLower().Contains(textToSearch)
                                   select i).ToArray();
                if (result.Length == 0)
                    return;

                listbox1.Items.Clear();
                foreach (var item in result)
                {
                    listbox1.Items.Add(item);
                }
                listbox1.Visibility = Visibility.Visible;
            }
        }

        private async void listbox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listbox1.SelectedItem != null)
            {
                CB.Text = null;
                sm = listbox1.SelectedItem as String;
                CB.IsEnabled = false;
                listbox1.Visibility = Visibility.Collapsed;
                if (sm != null)
                    await ShowWeather();
            }
        }

        private void MoreInfo_Click(object sender, RoutedEventArgs e)
        {
            if (CityTitle!=null)
                NavigationService.Navigate(new InDepthStatisticsPage(CityTitle, this));
            else {
                MessageBox.Show("Please choose a city", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private async void Update_Click(object sender, RoutedEventArgs e)
        {
            if (CityTitle != null)
            {
                CityName.Text = "Updating...";
                await ShowWeather();
                CityName.Text = CityTitle;
            }
            else
            {
                MessageBox.Show("Please choose a city", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Tab)
            {
                if (CityTitle != null)
                    NavigationService.Navigate(new InDepthStatisticsPage(CityTitle, this));
                else
                {
                    MessageBox.Show("Please choose a city", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
    }
}
