﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WeatherData;
using WeatherData.DTO;
using WeatherData.Interfaces;

namespace TeamProject
{
    /// <summary>
    /// Логика взаимодействия для InDepthStatisticsPage.xaml
    /// </summary>
    public partial class InDepthStatisticsPage : Page
    {
        string City;
        WeatherStatistics werst = new WeatherStatistics();
        IData wer = Factory.Instance.CreateWeatherData();
        public InDepthStatisticsPage(string City, WeatherStatistics werst)
        {
            InitializeComponent();
            this.City = City;
            this.werst = werst;
            this.Loaded += new RoutedEventHandler(Page_Loaded);
        }

        private async Task ShowForecast(object City)
        {
            try
            {
                var forecastData = await wer.GetForecast((string)City);

                if (forecastData != null)
                {
                    if (City != null)
                    {
                        var list1 = new List<ForecastItem>();
                        var list2 = new List<ForecastItem>();
                        var list3 = new List<ForecastItem>();
                        var list4 = new List<ForecastItem>();
                        var list5 = new List<ForecastItem>();
                        Helpers.GetRightData(forecastData.Forecast, ref list1, ref list2, ref list3, ref list4, ref list5);
                        var list1new = new List<ForecastResults>();
                        for (int i = 0; i < 9; i++)
                        {
                            list1new.Add(new ForecastResults
                            {
                                Main = list1[i].Main,
                                Weather = list1[i].Weather[0],
                                Time = list1[i].Time,
                                Wind = list1[i].Wind
                            });
                        }
                        foreach (var item in list1new)
                        {
                            item.Time = Helpers.GetDateAndTime(item.Time);
                            item.Main.Temp = Math.Round(item.Main.Temp);
                            item.Wind.WindSpeed = Math.Round(item.Wind.WindSpeed);
                        }
                        HoursWeather.ItemsSource = list1new;
                        if (list2.Count != 0 && list3.Count != 0 && list4.Count != 0 && list5.Count != 0)
                        {
                            ForecastWeather.ItemsSource = Helpers.ReturnListOfNewItemsForForecast(forecastData.Forecast, list1, list2, list3, list4, list5);
                            
                        }
                        CityText1.Text = (string)City;
                        CityText2.Text = (string)City;
                    }
                }
                else
                {
                    MessageBox.Show("No DATA returned", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    GridToday.Visibility = Visibility.Hidden;
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occured: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                GridToday.Visibility = Visibility.Hidden;
                return;
            }
        }

        //private void InfoSetter(ForecastItem item, TextBlock textBlock, string key)
        //{
        //    if (key == "Date")
        //        textBlock.Text = Helpers.GetDate(item.Time);
        //    if (key == "Time")
        //        textBlock.Text = Helpers.GetDateAndTime(item.Time);
        //    if(key == "Temp")
        //        textBlock.Text = $"{Math.Round(item.Main.Temp)}°";
        //    if (key == "WindSpeed")
        //        textBlock.Text = $"{Math.Round(item.Wind.WindSpeed)}m/s";
        //    if (key == "Description")
        //        textBlock.Text = item.Weather[0].Description;
        //}

        private async void Update_Click(object sender, RoutedEventArgs e)
        {
            CityText1.Text = "Updating...";
            CityText2.Text = "Updating...";
            await ShowForecast(City);
            CityText1.Text = City;
            CityText2.Text = City;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(werst);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await ShowForecast(City);
        }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Tab)
                NavigationService.Navigate(werst);
        }
    }
}
